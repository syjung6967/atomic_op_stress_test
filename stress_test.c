#define _GNU_SOURCE

#include <stdio.h>
#include <stdint.h>
#include <sched.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

/* _mm_pause() */
#include <xmmintrin.h>

#define MAX_FREQ 2000000000
#define MAX_CPUS 100
#define CORES_PER_NODE 6
#define GOAL_COUNT 50000000
#define DEMULT ((1 << 12) - 1)

#if 1
#define FAI
#else
#define FAI_CAS
#endif

#define PAUSE __asm__ __volatile__("pause")
#define PAUSE_SLEEP usleep(1)
#define FLUSH(addr) __asm__ __volatile__ \
	("clflush (%0)\n" : : "r"(addr) : "memory")
#define MFENCE __asm__ __volatile__("mfence")

void *shmem;
int correction;

struct test_data {
	int stop;
	int count;
	int dummy;
	int cpuid[MAX_CPUS];
} __attribute__((aligned(64)));

#if 1
static inline uint64_t rdtsc()
{
	uint32_t a, d;
	__asm __volatile("rdtsc" : "=a" (a), "=d" (d));
	return ((uint64_t) a) | (((uint64_t) d) << 32);
}
#else
/* This operation is not scalable. */
static inline uint64_t rdtsc()
{
	uint32_t lo, hi;
	__asm__ __volatile__ (
	  "xorl %%eax, %%eax\n"
	  "cpuid\n"
	  "rdtsc\n"
	  : "=a" (lo), "=d" (hi)
	  :
	  : "%ebx", "%ecx");
	return (uint64_t)hi << 32 | lo;
}
#endif

int setaffinity(int c)
{
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(c, &cpuset);
	return sched_setaffinity(0, sizeof(cpuset), &cpuset);
}

__attribute__((noinline)) void fake_func(int *fake)
{
	(*fake)++;
}

uint64_t test(int cpu, int num_cores)
{
	struct test_data *d = (struct test_data *) shmem;
	int cpuid = d->cpuid[cpu];
	int *count = &d->count;
	int old_val, old, new;

	int local_count = 0;
	uint64_t duration = 0, start, end;

	int i;
	int pause_loop;

	setaffinity(cpuid);

	while (d->stop);

	if (num_cores > CORES_PER_NODE) {
		pause_loop = 2 * CORES_PER_NODE;
	} else {
		pause_loop = 2 * (num_cores - 1);
	}
	pause_loop = 0;

	while (*count < GOAL_COUNT) {
		if (!(local_count & DEMULT))
			start = rdtsc();
#if defined(FAI)
		//__sync_fetch_and_add(count, 1);
		(*count)++;
#elif defined(FAI_CAS)
		old = *count;
		while (1) {
			new = old + 1;
			old_val = __sync_val_compare_and_swap(count, old, new);
			if (old_val == old)
				break;
			old = old_val;
			_mm_pause();
		}
#endif
		if (!(local_count & DEMULT)) {
			end = rdtsc();
			duration += end - start - correction;
		}

#if defined(DUMMY)
		d->dummy += 0x93f3;
#endif
#if defined(FUNC_CALL)
		fake_func(&d->dummy);
#endif

		/* for balancing */
		for (i = 1; i < pause_loop; i++) {
			_mm_pause();
		}

		local_count++;
	}

	d->cpuid[cpu] = -1;
	printf("dummy: %d\n", d->dummy);
	printf("CPU #%3d: local_count = %d\n", cpuid, local_count);
	printf("duration[%d]: %llu\n", cpuid, ((unsigned long long) duration * DEMULT));
	printf("latency[%d]: %llu\n", cpuid, ((unsigned long long) duration * DEMULT) / local_count);

	return duration;
}

int rdtsc_correction()
{
	int i;
	uint64_t duration = 0, start, end;

#define CORRECTION_LOOP 20000
	for (i = 0; i < CORRECTION_LOOP; i++) {
		start = rdtsc();
		end = rdtsc();
		duration += end - start;
	}

	return ((int) duration) / CORRECTION_LOOP;
}

void pre_work(int num_cores)
{
	int shmem_size = sizeof(struct test_data);
	struct test_data *test_data;
	int i;

	shmem = mmap(0, shmem_size, PROT_READ | PROT_WRITE,
				 MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	if (shmem == MAP_FAILED) {
		fprintf(stderr, "mmap() error!\n");
		exit(0);
	}
	memset(shmem, 0, shmem_size);

	test_data = (struct test_data *) shmem;
	test_data->stop = 1;
	test_data->count = 0;

#if 0
	for (i = 0; i < num_cores; i++) {
		test_data->cpuid[i] = i;
	}
#else
	for (i = 0; i < num_cores; i++) {
		test_data->cpuid[i] = (i + 6) % 12;
	}
#endif

	correction = rdtsc_correction();
	printf("rdtsc correction: %d\n", correction);
}

int main(int argc, const char *argv[])
{
	uint64_t duration[MAX_CPUS];
	int num_cores;
	int i;

	uint64_t start, end;

	sscanf(argv[1], "%d", &num_cores);
	printf("# of threads: %d\n", num_cores);
	pre_work(num_cores);

	for (i = 1; i < num_cores; i++) {
		pid_t p = fork();
		if (p < 0) {
			fprintf(stderr, "fork() error!\n");
		} else if (!p) {
			duration[i] = test(i, num_cores);
			exit(0);
		}
	}

	start = rdtsc();

	/* process all threads simultaneously */
	((struct test_data *) shmem)->stop = 0;
	duration[0] = test(0, num_cores);

	end = rdtsc();

	printf("count: %d\n", ((struct test_data *) shmem)->count);
	printf("elapsed time: %lf\n", (end - start) / (double) MAX_FREQ);

	return 0;
}
